angular.module('DDApp', [
	'ngRoute', 
	'ngSanitize',
	'appRoutes', 
	'MainCtrl', 
	'PagesCtrl',
	'duScroll'
])
.value('duScrollDuration', 1000)
.value('duScrollGreedy', true); 