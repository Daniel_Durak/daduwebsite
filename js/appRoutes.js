// public/js/appRoutes.js
    angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'views/main.html',
            controller: 'MainController'
        })
        .when('/styleguide', {
            templateUrl: 'views/styleguide.html',
            controller: 'MainController'
        })
        .when('/pages', {
            templateUrl: 'views/pages/index.html',
            controller: 'PagesController'
        })
        .when('/template', {
            templateUrl: 'views/template/index.html',
            controller: 'PagesController'
        });

    $locationProvider.html5Mode(true);

}]);


