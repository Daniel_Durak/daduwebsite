angular.module('PagesCtrl', []).controller('PagesController', function($scope, $window, $http, $rootScope) {
	var main = $scope;
	main.activePage = 1;
	main.aktModul = 1;
	
	main.pages = [
		{
			"id"		: 	1,
			"title" 	: 	"Startseite"
		},
		{
			"id"		: 	2,
			"title" 	: 	"Referenzen",
			"video" 	: 	"../video/hero.mp4"
		},
		{
			"id"		: 	3,
			"title" 	: 	"Projekte",
			"video" 	: 	"../video/thrice.mp4"
		},
		{
			"id"		: 	4,
			"title" 	: 	"Über uns",
			"modules" 	: 	[
				{
					"name"		: "thrice",
					"images"	: [
						"../img/module-thrice_01.jpg",
						"../img/module-thrice_02.jpg"
					]
				},
				{
					"name"		: "hero",
					"images"	: [
						"../img/module-hero_01.jpg",
						"../img/module-hero_02.jpg"
					]
				}
			]
		},
		{
			"id"		: 	5,
			"title" 	: 	"Kontakt"
		}
	];

	main.changePage = function (id){
		main.activePage=id;
	}
	main.nextModulImage = function (){
		main.aktModul = main.aktModul + 1;
	}
		


});