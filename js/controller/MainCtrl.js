angular.module('MainCtrl', []).controller('MainController', function($scope, $window, $http, $rootScope) {

	$scope.name="Daniel";
	$scope.showBsp = true;
	$scope.menuopen = false;
	$rootScope.bgimage = {"background-image":"url('img/seq/0090.jpg')"};

    var position = $window.scrollY;
	var count = 90;
		
	angular.element($window).bind('scroll', scrollFunction);

	
	$scope.menu = [
		{
			"title" 	: "Über mich",
			"id"		: "ueber_mich"
		},
		{
			"title" 	: "Schulische Ereignisse",
			"id"		: "schulische_ereignisse"
		},
		{
			"title" 	: "Passionen",
			"id"		: "passionen"
		},
		{
			"title" 	: "Werdegang",
			"id"		: "werdegang"
		},
		{
			"title" 	: "Arbeitsproben",
			"id"		: "arbeitsproben"
		}
	];
	$scope.about = [
		{
			"title"		: "Staats&shy;angehörigkeit",
			"text"		: "deutsch" 
		},
		{
			"title"		: "Sprachen",
			"text"		: "Deutsch, Englisch, Italienisch"
		},
		{
			"title"		: "Hobbies",
			"text" 		: "Musik, Modellbau, Blender, Web-Applikationen"
		}
	];
	$scope.preferences = [
		{
			"title"	: 	"HTML5",
			"src"	: 	"../img/svg/html5_logo.svg"
		},
		{
			"title"	: 	"CSS3",
			"src"	: 	"../img/svg/css3_logo.svg"
		},
		{
			"title"	: 	"Bootstrap",
			"src"	: 	"../img/svg/bootstrap_logo.svg"
		},
		{
			"title"	: 	"JavaScript",
			"src"	: 	"../img/svg/javascript_logo.svg"
		},
		{
			"title"	: 	"AngularJS",
			"src"	: 	"../img/svg/angularjs_logo.svg"
		},
		{
			"title"	: 	"NodeJS",
			"src"	: 	"../img/svg/nodejs_logo.svg"
		},
		{
			"title"	: 	"Java",
			"src"	: 	"../img/svg/java_logo.svg"
		},
		{
			"title"	: 	"Oracle",
			"src"	: 	"../img/svg/oracle_logo.svg"
		}
	];


	$scope.timeline = [
		{
			"title"		: 	"Geburtstag",
			"icon" 		: 	"leanpub",
			"text"		: 	"Geboren am 25. März in Böblingen",
			"date" 		: 	"1993"
		},
		{
			"title"		: 	"Grundschule",
			"icon" 		: 	"leanpub",
			"text"		: 	"an der Eduard-Mörike-Schule in Böblingen",
			"date" 		: 	"1999 <span>(Sep&shy;tember 1999 bis Au&shy;gust 2003)</span>"
		},
		{
			"title"		: 	"Realschule",
			"icon" 		: 	"leanpub",
			"text"		: 	"an der Friedrich-Schiller-Schule in Böblingen",
			"date" 		: 	"2003 <span>(Sep&shy;tember 2003 bis Au&shy;gust 2009)</span>"
		},
		{
			"title"		: 	"Gymnasium",
			"icon" 		: 	"leanpub",
			"text"		: 	"an der Gottlieb-Daimler-Schule 1 in Sindelfingen",
			"date" 		: 	"2009 <span>(Sep&shy;tember 2009 bis Au&shy;gust 2012)</span>"
		},
		{
			"title"		: 	"FSJ",
			"icon" 		: 	"leanpub",
			"text"		: 	"an der Käthe-Kollwitz-Schule in Böblingen",
			"date" 		: 	"2012 <span>(Sep&shy;tember 2012 bis Au&shy;gust 2013)</span>"
		},
		{
			"title"		: 	"Schulische Ausbildung",
			"icon" 		: 	"leanpub",
			"text"		: 	"an der Akademie für Datenverarbeitung in Böblingen",
			"date" 		: 	"2013 <span>(Ok&shy;tober 2013 bis heute)</span>"
		}
	];

	$scope.arbeitsproben = [
		{
			"img" : "../img/eshop.png",
			"title" : "E-Shop",
			"description" : "Teamprojekt im Rahmen des Unterrichts an der ADV (WPR - Webprogrammierung)",
			"tech" : ["../img/svg/html5_logo.svg", "../img/svg/css3_logo.svg", "../img/svg/java_logo.svg"]

		},
		{
			"img" : "../img/nonplus.png",
			"title" : "Webbasiertes Präsentationsframework",
			"description" : "Praktikumsarbeit im Rahmen des Praktikumsjahres der ADV - Von der Konzeption bis zur Umsetzung eines webbasierten Präsentationsframeworks",
			"tech" : ["../img/svg/html5_logo.svg", "../img/svg/css3_logo.svg", "../img/svg/javascript_logo.svg", "../img/svg/angularjs_logo.svg"]

		},
		{
			"img" : "../img/barcamp.png",
			"title" : "Barcamp Tool",
			"description" : "Internes Agenturprojekt der Youngstars im Rahmen des Praktikumsjahres in der synergetic agency AG",
			"tech" : ["../img/svg/html5_logo.svg", "../img/svg/css3_logo.svg", "../img/svg/java_logo.svg", "../img/svg/javascript_logo.svg"]

		},
		{
			"img" : "../img/eshop.png",
			"title" : "Titel 4",
			"description" : "Eine kurze Beschreibung",
			"tech" : ["../img/svg/angularjs_logo.svg", "../img/svg/javascript_logo.svg", "../img/svg/nodejs_logo.svg"]

		}
	];

	function scrollFunction(e) { 
		var scroll = e.path[0].scrollY;
		$rootScope.bgimage = {"background-image":"url('img/seq/00"+count+".jpg')"};
		if (scroll > position) {
			console.log("DOWN");

		}
		else{
			console.log("UP");
		}
		position = scroll;
		count++;
	};

	$scope.menuclose = function(){
		console.log("close");
		$scope.menuopen = !$scope.menuopen;
	}


});